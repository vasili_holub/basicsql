﻿--Задание 2. Отработка SQL запросов на объединение таблиц и агрегацию
--Задание 2.3. Использование JOIN

--1. Определить продавцов, которые обслуживают регион 'Western' (таблица Region).

 SELECT Employees.[FirstName] + ' ' + Employees.[LastName] AS Seller
   FROM Employees LEFT JOIN Region ON Employees.ReportsTo = Region.RegionID
  WHERE ((Region.RegionDescription)='Western');
