﻿--Задание 2. Отработка SQL запросов на объединение таблиц и агрегацию
--Задание 2.4. Использование подзапросов 

--2. Выдать всех продавцов, которые имеют более 150 заказов. Использовать вложенный SELECT.

    SELECT e.[LastName] + ' ' + e.[FirstName] as Seller
      FROM Employees as e
     WHERE e.EmployeeID
        IN
            (SELECT Orders.EmployeeID
               FROM Orders
           GROUP BY Orders.EmployeeID
             HAVING (Count(Orders.OrderId) > 150));