﻿--Задание 2. Отработка SQL запросов на объединение таблиц и агрегаци
--Задание 2.1. Использование агрегатных функций (SUM, COUNT)

--3. По таблице Orders найти количество различных покупателей (CustomerID), сделавших заказы. 
--Использовать функцию COUNT и не использовать предложения WHERE и GROUP

  SELECT COUNT(DISTINCT o.CustomerId) AS CustomerNumber
    FROM Orders AS o;