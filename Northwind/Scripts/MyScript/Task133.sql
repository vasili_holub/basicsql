﻿--Задание 1.3. Использование оператора BETWEEN, DISTINCT

--3. Выбрать всех заказчиков из таблицы Customers, у которых название страны 
--начинается на буквы из диапазона b и g, не используя оператор BETWEEN

   SELECT c.CustomerId, c.Country
     FROM Customers AS c
    WHERE
          (SUBSTRING(c.Country, 1, 1) >= 'b') AND
          (SUBSTRING(c.Country, 1, 1) <= 'g')
 ORDER BY c.Country;