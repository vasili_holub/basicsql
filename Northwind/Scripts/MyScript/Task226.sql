﻿--Задание 2. Отработка SQL запросов на объединение таблиц и агрегацию
--Задание 2.2. Соединение таблиц, использование агрегатных функций и предложений GROUP BY и HAVING

--6. По таблице Employees найти для каждого продавца его руководителя.

    SELECT (e.FirstName + ' ' + e.LastName) Employee, 
    CASE 
      WHEN (SELECT (b.FirstName + ' ' + b.LastName) FROM Employees b WHERE b.EmployeeId = e.ReportsTo) is null    THEN 'He(she) is boss himself(herself).'
      WHEN (SELECT (b.FirstName + ' ' + b.LastName) FROM Employees b WHERE b.EmployeeId = e.ReportsTo) is not null THEN (SELECT (b.FirstName + ' ' + b.LastName) FROM Employees b WHERE b.EmployeeId = e.ReportsTo)
    END AS Boss
      FROM Employees e;
