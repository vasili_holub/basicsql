﻿--Задание 2. Отработка SQL запросов на объединение таблиц и агрегацию
--Задание 2.3. Использование JOIN

--2. Выдать в результатах запроса имена всех заказчиков из таблицы Customers и
--суммарное количество их заказов из таблицы Orders. Принять во внимание, что у 
--некоторых заказчиков нет заказов, но они также должны быть выведены
--в результатах запроса. Упорядочить результаты запроса по возрастанию количества заказов.

   SELECT CompanyName AS Customer, Count(Orders.OrderId) AS [Orders Count]
     FROM Customers LEFT JOIN Orders ON Customers.CustomerId = Orders.CustomerId
 GROUP BY CompanyName
 ORDER BY Count(Orders.OrderId);
