﻿--Задание 1.3. Использование оператора BETWEEN, DISTINCT

--2. Выбрать всех заказчиков из таблицы Customers, у которых название страны начинается 
--на буквы из диапазона b и g. Использовать оператор BETWEEN. Проверить, что в результаты 
--запроса попадает Germany. Запрос должен возвращать только колонки CustomerID и Country
--и отсортирован по Country.

  SELECT c.CustomerId, c.Country
    FROM Customers AS c
   WHERE SUBSTRING(c.Country, 1, 1) BETWEEN 'b' AND 'g'
ORDER BY c.Country;