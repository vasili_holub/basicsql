﻿--Задание 1.1. Простая фильтрация данных.

--2. Написать запрос, который выводит только недоставленные заказы из таблицы Orders. 
--В результатах запроса возвращать для колонки ShippedDate вместо значений NULL строку ‘Not Shipped’ 
--(использовать системную функцию CASЕ). Запрос должен возвращать только колонки OrderID и ShippedDate.

   SELECT OrderID, 
   CASE 
     WHEN ShippedDate is NULL THEN 'Not shipped'
     ELSE 'Unknown'
   END as ShippedDate
     FROM Orders
    Where ShippedDate is NULL;