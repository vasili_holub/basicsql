﻿--Задание 2. Отработка SQL запросов на объединение таблиц и агрегацию
--Задание 2.2. Соединение таблиц, использование агрегатных функций и предложений GROUP BY и HAVING

--4. Найти покупателей и продавцов, которые живут в одном городе. Если в городе живут 
--только один или несколько продавцов, или только один или несколько покупателей, то информация о таких покупателя и
--продавцах не должна попадать в результирующий набор. Не использовать конструкцию JOIN.

            (SELECT (e.FirstName + ' ' + e.LastName), e.City
                FROM Employees as e
               WHERE e.City IN 
                               (SELECT DISTINCT c.City 
                                           FROM Customers AS c))
    UNION
            (SELECT c.CompanyName, c.City
               FROM Customers as c
              WHERE c.City IN 
                               (SELECT DISTINCT e.City 
                                           FROM Employees as e));