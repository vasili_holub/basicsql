﻿--Задание 2. Отработка SQL запросов на объединение таблиц и агрегацию
--Задание 2.2. Соединение таблиц, использование агрегатных функций и предложений GROUP BY и HAVING

--3. По таблице Orders найти количество заказов, сделанных каждым продавцом и для каждого покупателя. 
--Необходимо определить это только для заказов, сделанных в 1998 году. 

   SELECT o.EmployeeId Employee, o.CustomerId Customer, COUNT(o.OrderId) Amount
     FROM Orders o 
    WHERE YEAR(o.OrderDate) = 1998
 GROUP BY o.EmployeeId, o.CustomerId; 