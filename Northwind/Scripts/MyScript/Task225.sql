﻿--Задание 2. Отработка SQL запросов на объединение таблиц и агрегацию
--Задание 2.2. Соединение таблиц, использование агрегатных функций и предложений GROUP BY и HAVING

--5. Найти всех покупателей, которые живут в одном городе
    
  SELECT Customers.CompanyName
    FROM Customers
   WHERE Customers.City IN
         (SELECT c.City
            FROM Customers AS c
        GROUP BY c.City
          HAVING Count(c.CustomerID)>1);
