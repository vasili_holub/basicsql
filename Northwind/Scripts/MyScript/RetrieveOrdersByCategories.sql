﻿		SELECT Categories.CategoryName, 
		       Products.ProductId, 
			   Products.ProductName, 
			   OrderDetails.OrderId, 
			   Customers.CompanyName, 
			   OrderDetails.UnitPrice, 
			   OrderDetails.Quantity, 
			   OrderDetails.Discount
          FROM (((Orders INNER JOIN OrderDetails ON Orders.OrderId = OrderDetails.OrderId) 
		 INNER JOIN Customers ON Orders.CustomerId = Customers.CustomerId)
		 INNER JOIN Products ON OrderDetails.ProductId = Products.ProductId) 
		 INNER JOIN Categories ON Products.CategoryId = Categories.CategoryId
         WHERE (((Categories.CategoryId)=2));