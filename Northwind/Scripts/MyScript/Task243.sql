﻿--Задание 2. Отработка SQL запросов на объединение таблиц и агрегацию
--Задание 2.4. Использование подзапросов 

--3. Выдать всех заказчиков (таблица Customers), которые не имеют ни одного 
--заказа (подзапрос по таблице Orders). Использовать оператор EXISTS.

   SELECT CompanyName 
     FROM Customers
    WHERE NOT EXISTS
          (SELECT OrderID 
             FROM Orders 
            WHERE Orders.CustomerID = Customers.CustomerID);