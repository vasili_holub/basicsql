﻿--Задание 1.1. Простая фильтрация данных.

--3. Выбрать в таблице Orders заказы, которые были доставлены после 6 мая 1998 года (ShippedDate) 
--не включая эту дату или которые еще не доставлены. В запросе должны возвращаться только колонки OrderID 
--(переименовать в Order Number) и ShippedDate (переименовать в Shipped Date). В результатах запроса 
--возвращать для колонки ShippedDate вместо значений NULL строку ‘Not Shipped’, 
--для остальных значений возвращать дату в формате по умолчанию.

    SELECT o.OrderId AS [Order Number],
    CASE 
      WHEN o.ShippedDate is null THEN 'Not shipped' 
      WHEN o.ShippedDate is not null THEN CAST(o.ShippedDate AS nvarchar)
    END AS [Shipped Date]
      FROM Orders o
     WHERE o.ShippedDate > '1998-05-06' OR o.ShippedDate is null;