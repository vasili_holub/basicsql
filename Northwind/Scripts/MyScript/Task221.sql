﻿--Задание 2. Отработка SQL запросов на объединение таблиц и агрегацию
--Задание 2.2. Соединение таблиц, использование агрегатных функций и предложений GROUP BY и HAVING

--1. По таблице Orders найти количество заказов с группировкой по годам. 
--В результатах запроса надо возвращать две колонки c названиями Year и Total. 
--Написать проверочный запрос, который вычисляет количество всех заказов

   SELECT COUNT(o.OrderId) AS 'Total', Year(o.OrderDate) AS 'Year', 
          (SELECT COUNT(OrderId) 
             FROM Orders) as 'TotalAmmount'
     FROM Orders AS o
 GROUP BY YEAR(o.OrderDate);