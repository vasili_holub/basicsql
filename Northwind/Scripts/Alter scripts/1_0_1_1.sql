﻿IF NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_NAME = N'Cards')
BEGIN
    CREATE TABLE Cards(
	[CardID] [int] IDENTITY(1,1) NOT NULL,
	[CardNumber] [nvarchar](30) NOT NULL,
	[Expired] [datetime] NULL,
	[CardHolder] [nvarchar](50) NULL,
	[EmployeeID] [int] NULL,
 CONSTRAINT [CardID] PRIMARY KEY CLUSTERED 
(
	[CardID] ASC
)
)
END