﻿IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_NAME = N'Regions')) AND
  (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_NAME = N'Region'))
BEGIN
    exec sp_rename Region, Regions;
END

IF NOT EXISTS(SELECT * FROM sys.columns 
          WHERE Name = N'Founded'
          AND Object_ID = Object_ID(N'Customers'))
BEGIN
    ALTER TABLE Customers
    ADD Founded datetime;
END